package com.shift4.livecoding;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LiveCodingApp {
    public static void main(String[] args) {
        SpringApplication.run(LiveCodingApp.class, args);
    }
}
